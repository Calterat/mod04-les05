// TODO: Create an array with five question objects

const questions = [
    {
        q: "Do you have a name?",
        a: true
    },
    {
        q: "Do you attend Bootcamp?",
        a: true
    },
    {
        q: "Do you have a favorite color?",
        a: true
    },
    {
        q: "Do you know the capitol of Assyria?",
        a: false
    },
    {
        q: "Do you know the air speed velocity of an unnamed swallow?",
        a: false
    }
];

// TODO: Create a variable to keep track of the score
let score = 0;
// TODO: Iterate over the questions array and display each question in a confirmation box
for (i = 0; i < questions.length; ++i) {
    // TODO: Check the user's answer against the correct answer
    // TODO: Alert the user if they are correct or wrong. Increment the score accordingly
    // TODO: Alert the user if they are correct or wrong. Increment the score accordingly
    if ((confirm(questions[i].q)) === questions[i].a) {
        alert("You are correct with question " + (i+1) + "!");
        ++score;
    } else {
        alert("You are wrong with question " + (i+1) + "!");
    }
}
// TODO: At the end of the game, alert the user with the final score
alert("Your final score is " + score + "!");
